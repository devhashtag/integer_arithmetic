using System;
using System.IO;
using System.Collections.Generic;
namespace integer_arithmetic
{
    class Test
    {
        public void Testing ()
        {
            int radix = 10; //variable base for the numbers
            int digits = 10; //variable amount of digits for the numbers
            int modDigits = 5; //variable amount of digits for the mod
            List<int> aList = new List<int>();
            List<int> bList = new List<int>();
            List<int> mList = new List<int>();
            var rand = new Random();

            for (int i = 0; i < digits; i++) {
                aList.Add(rand.Next(radix));
                bList.Add(rand.Next(radix));
            }

            for (int j = 0; j < modDigits; j++) {
                mList.Add(rand.Next(radix));
            }

            Number a = Number.fromString("26146198083132013202780", 10);
            Number b = Number.fromString("944095899986950667116291", 10);
            Number m = Number.fromString("39722", 10);
            
            (Number d, Number e, Number f) = Number.extendedEucledian(a, b);
            Console.WriteLine("gcd(" + a.toString() + ", " + b.toString() + ") = " + d.toString() + " with coefficients " + e.toString() + " and " + f.toString());
            Console.WriteLine(a.toString() + " + " + b.toString() + " mod " + m.toString() + " = " + Number.modAddition(a,b,m).toString());
            Console.WriteLine(a.toString() + " - " + b.toString() + " mod " + m.toString() + " = " + Number.modSubtraction(a,b,m).toString());
            Console.WriteLine(a.toString() + " mod " + m.toString() + " = " + (a % m).toString());
            Console.WriteLine(a.toString() + " - " + b.toString() + " = " + (a - b).toString());
            Console.WriteLine(a.toString() + " + " + b.toString() + " = " + (a + b).toString());
            Console.WriteLine(a.toString() + " * " + b.toString() + " = " + (a * b).toString());
            Console.WriteLine(a.toString() + " karatsuba " + b.toString() + " = " + Number.karatsuba(a, b).toString());
            Console.WriteLine(a.toString() + " * " + b.toString() + " mod " + m.toString() + " = " + Number.modMultiplication(a,b,m).toString());
        }
    }
}