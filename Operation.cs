using System;
using System.Collections.Generic;

namespace integer_arithmetic {
    class Operation {

        public int OP_CODE;
        int radix;
        public Number x;
        public Number y;
        public Number m;

        // result of the operation
        private Number result;

        // result for operations that return more than one result
        private Number[] results;
        private int additions = 0;
        private int multiplications = 0;

        public Operation(int OP_CODE, Number x, Number y = null, Number m = null) {
            this.OP_CODE = OP_CODE;
            this.x = x;
            this.y = y;
            this.m = m;
            this.radix = this.x.getRadix();

            // check if the right Numbers are not null

            // all operations where x and y need to be defined
            if (OP_CODE == (int)Global.OP_CODES.ADD ||
                OP_CODE == (int)Global.OP_CODES.SUBTRACT ||
                OP_CODE == (int)Global.OP_CODES.MULTIPLY ||
                OP_CODE == (int)Global.OP_CODES.KARATSUBA ||
                OP_CODE == (int)Global.OP_CODES.EUCLID)
            {
                if (x == null || y == null) {
                    throw new Exception("x and y need to be defined for op-code: " + OP_CODE.ToString());
                }
            }

            // all operations where x, y and m need to be defined
            if (OP_CODE == (int)Global.OP_CODES.MODULAR_ADD ||
                OP_CODE == (int)Global.OP_CODES.MODULAR_SUBTRACT ||
                OP_CODE == (int)Global.OP_CODES.MODULAR_MULTIPLY)
            {
                if (x == null || y == null || m == null) {
                    throw new Exception("x, y and m need to be defined for op-code: " + OP_CODE.ToString());
                }
            }

            // all operations where x and m need to be defined
            if (OP_CODE == (int)Global.OP_CODES.REDUCE ||
                OP_CODE == (int)Global.OP_CODES.INVERSE)
            {
                if (x == null || m == null) {
                    throw new Exception("x and m need to be defined for op-code: " + OP_CODE.ToString());
                }
            }
        }

        public void execute() {
            // set additions and multiplications to the global count
            this.additions = Global.additions;
            this.multiplications = Global.multiplications;

            switch (this.OP_CODE) {
                case (int)Global.OP_CODES.ADD:
                    result = x + y;
                    break;
                case (int)Global.OP_CODES.SUBTRACT:
                    result = x - y;
                    break;
                case (int)Global.OP_CODES.MULTIPLY:
                    result = x * y;
                    break;
                case (int)Global.OP_CODES.KARATSUBA:
                    result = Number.karatsuba(x, y);
                    break;
                case (int)Global.OP_CODES.REDUCE:
                    result = x % m;
                    break;
                case (int)Global.OP_CODES.INVERSE:
                    result = Number.modInverse(x, m);
                    break;
                case (int)Global.OP_CODES.EUCLID:
                    results = new Number[3];
                    (results[0], results[1], results[2]) = Number.extendedEucledian(x, y);
                    break;
                case (int)Global.OP_CODES.MODULAR_ADD:
                    result = Number.modAddition(x, y, m);
                    break;
                case (int)Global.OP_CODES.MODULAR_SUBTRACT:
                    result = Number.modSubtraction(x, y, m);
                    break;
                case (int)Global.OP_CODES.MODULAR_MULTIPLY:
                    result = Number.modMultiplication(x, y, m);
                    break;
                default:
                    throw new Exception("Unsupported operation, please check if your input file is correct");
            }

            // set additions and multiplications to the difference of them before and after execution (e.g. how many additions/multiplications the algorithms used)
            this.additions = Global.additions - this.additions;
            this.multiplications = Global.multiplications - this.multiplications;
        }

        public Number getResult() {
            return this.result;
        }

        public string generateOutput() {
            // operation has not been executed or failed
            // if (result == null) {
            //     throw new Exception("Cannot generate output because the operation has not yet been executed");
            // }

            List<string> output = new List<string>();

            // write the radix
            output.Add(Global.TOKEN_SYMBOLS[(int) Global.TOKENS.RADIX] + " " + this.radix);
            // write the operation
            output.Add(Global.OPERATION_SYMBOLS[this.OP_CODE]);
            // write x
            output.Add(Global.TOKEN_SYMBOLS[(int) Global.TOKENS.X] + " " + x.toString());
            // write y if necessary
            if (y != null) {
                output.Add(Global.TOKEN_SYMBOLS[(int) Global.TOKENS.Y] + " " + y.toString());
            }
            // write m if necessary
            if (m != null) {
                output.Add(Global.TOKEN_SYMBOLS[(int) Global.TOKENS.M] + " " + m.toString());
            }

            // write answer
            switch(OP_CODE) {
                case (int) Global.OP_CODES.EUCLID:
                    output.Add("[answ-d] " + results[0].toString());
                    output.Add("[answ-a] " + results[1].toString());
                    output.Add("[answ-b] " + results[2].toString());
                    break;
                case (int) Global.OP_CODES.INVERSE:
                    if (result != null) {
                        output.Add("[answer] " + result.toString());
                    } else {
                        output.Add("inverse does not exist");
                    }
                    break;
                default:
                    output.Add("[answer] " + result.toString());
                    break;
            }

            // if the operation is an arithmetic operation, we need to output count-add and count-mult
            if (OP_CODE == (int) Global.OP_CODES.ADD ||
                OP_CODE == (int) Global.OP_CODES.SUBTRACT ||
                OP_CODE == (int) Global.OP_CODES.MULTIPLY ||
                OP_CODE == (int) Global.OP_CODES.KARATSUBA ||
                OP_CODE == (int) Global.OP_CODES.MODULAR_ADD ||
                OP_CODE == (int) Global.OP_CODES.MODULAR_SUBTRACT ||
                OP_CODE == (int) Global.OP_CODES.MODULAR_MULTIPLY)
            {
                // write count-add
                output.Add("[count-add] " + this.additions);
                // write count-mult
                output.Add("[count-mult] " + this.multiplications);
            }

            return String.Join(Environment.NewLine, output.ToArray());
        }
    }
}