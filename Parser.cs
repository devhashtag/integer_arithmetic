using System;
using System.Collections.Generic;
using System.IO;

namespace integer_arithmetic {
    class Parser {
        private string filename = "";

        public Parser(string filename) {
            this.filename = filename;
        }

        public List<Operation> parse() {
            // read file
            List<string> lines = this.readFile(this.filename);
            // tokenize lines
            List<Token> tokens = this.tokenize(lines);
            // generate operations from tokens
            List<Operation> operations = this.parse(tokens);

            return operations;
        }

        // Reads a file from a location and returns a list of lines (strings) that contain its contents
        private List<string> readFile(string filename) {
            // will contain the content of the file, as a list of strings (split on linebreaks)
            List<string> contents = new List<string>();

            // create filestream
            FileStream fs = new FileStream(this.filename, FileMode.Open);

            // read file line by line
            using (StreamReader reader = new StreamReader(fs)) {
                string line;

                // keep reading until there is nothing to read
                while ((line = reader.ReadLine()) != null) {
                    contents.Add(line);
                }
            }

            return contents;
        }

        // Converts a list of string to a list of tokens (does not modify original list of strings)
        private List<Token> tokenize(List<string> lines) {
            List<Token> tokens = new List<Token>();

            for (int i = 0; i < lines.Count; i++) {
                // shortcut
                string s = lines[i];
                // we remove all tabs and spaces, because they don't mean anything
                s = s.Replace(" ", "");
                s = s.Replace("\t", "");

                // if the string contains a #, it means that the rest of the line is a comment and we can remove it
                int loc = s.IndexOf('#');
                // if it contains a #, remove it and everything after it
                if (loc != -1) {
                    s = s.Remove(loc);
                }

                // if the line is empty, we need to skip it
                if (s.Length == 0) {
                    continue;
                }

                foreach (string symbol in Global.TOKEN_VALUES.Keys) {
                    if (s.StartsWith(symbol)) {
                        s = s.Substring(symbol.Length);

                        Token t = new Token(Global.TOKEN_VALUES[symbol], s);
                        tokens.Add(t);
                        break;
                    }
                }
            }

            return tokens;
        }

        // Generates a list of operations from a list of tokens
        private List<Operation> parse(List<Token> tokens) {
            List<Operation> operations = new List<Operation>();

            /*
                We expect operations to be in this order, and assign a state to each step:
                radix           STATE: 0
                <operation>     STATE: 1
                [x]             STATE: 2
                [y]*            STATE: 3
                [m]*            STATE: 3

                * means optional
             */

            int state = 0;

            // temporary variables that will be used to construct an operation
            int radix = 0;
            int opToken = 0;

            // initialize x to Zero and y and m to null (y and m are optional)
            Number x = Number.Zero();
            Number y = null;
            Number m = null;

            for (int i = 0; i < tokens.Count; i++) {
                Token t = tokens[i];

                switch (state) {
                    case 0: // token should be RADIX
                        if (t.getToken() != (int) Global.TOKENS.RADIX) {
                            // throw error
                            throw new Exception("Expected RADIX but got " + t.getToken());
                        }

                        radix = Int32.Parse(t.getArgument());
                        // advance state
                        state++;
                        break;
                    case 1: // token should be <operation>
                        // check if the instruction is an operation
                        if (!Global.isOperation(t.getToken())) {
                            // throw error
                            throw new Exception("Expected an operation but got " + t.getToken());
                        }

                        // Gets an op_code from an instruction, if it has one
                        opToken = t.getToken();
                        // advance state
                        state++;
                        break;
                    case 2: // token should be X
                        if (t.getToken() != (int) Global.TOKENS.X) {
                            // throw error
                            throw new Exception("Expected X but got " + t.getToken());
                        }

                        // we need to convert the argument of the token to a Number instance
                        // because the state is 2, we can be sure that the radix has already been set
                        x = Number.fromString(t.getArgument(), radix);
                        // advance state
                        state++;
                        break;
                    case 3: // token can be Y or M (optional)
                        // if token is Y, we set y
                        if (t.getToken() == (int) Global.TOKENS.Y) {
                            y = Number.fromString(t.getArgument(), radix);
                        }

                        // if token is M, we set m
                        if (t.getToken() == (int) Global.TOKENS.M) {
                            m = Number.fromString(t.getArgument(), radix);                            
                        }

                        // if the next token is neither Y or M, it means that a new operation must have started or the file has ended
                        // therefore, if that is the case, we will bundle the gathered data in an Operation object and reset the state

                        // since we also need to generate an operation if we consumed the last token, we check for that as well
                        if ((t.getToken() != (int) Global.TOKENS.Y && t.getToken() != (int) Global.TOKENS.M) || i == tokens.Count - 1) {
                            // create new Operation
                            operations.Add(createOperation(opToken, x, y, m));
                            // reset state to 0 for next operation
                            state = 0;
                            // we need to decrease i by 1, because t is the token that should be handled in the 0-case;
                            // if we do not decrease i by 1, the parses will skip this token which we do not want
                            // this has one exception: i should not be decreased if we just consumed the last token (we dont want to start a new 'iteration' of the switch statement)
                            i = i == tokens.Count - 1 ? i: i - 1;
                            // reset all variables
                            opToken = 0;
                            x = Number.Zero();
                            y = null;
                            m = null;
                        }
                        break;
                }
            }
            return operations;
        }

        private Operation createOperation(int opToken, Number x, Number y, Number m) {
            // if m does not exist, we use the default operation code
            // if it does, then we use the modular operation code

            int opcode = 0;

            if (m == null) {
                switch (opToken) {
                    case (int) Global.TOKENS.ADD: opcode = (int) Global.OP_CODES.ADD; break;
                    case (int) Global.TOKENS.SUBTRACT: opcode = (int) Global.OP_CODES.SUBTRACT; break;
                    case (int) Global.TOKENS.MULTIPLY: opcode = (int) Global.OP_CODES.MULTIPLY; break;
                    case (int) Global.TOKENS.KARATSUBA: opcode = (int) Global.OP_CODES.KARATSUBA; break;
                    case (int) Global.TOKENS.EUCLID: opcode = (int) Global.OP_CODES.EUCLID; break;
                }
            } else {
                switch (opToken) {
                    case (int) Global.TOKENS.ADD: opcode = (int) Global.OP_CODES.MODULAR_ADD; break;
                    case (int) Global.TOKENS.SUBTRACT: opcode = (int) Global.OP_CODES.MODULAR_SUBTRACT; break;
                    case (int) Global.TOKENS.KARATSUBA: opcode = (int) Global.OP_CODES.MODULAR_MULTIPLY; break;
                    case (int) Global.TOKENS.MULTIPLY: opcode = (int) Global.OP_CODES.MODULAR_MULTIPLY; break;
                    case (int) Global.TOKENS.REDUCE: opcode = (int) Global.OP_CODES.REDUCE; break;
                    case (int) Global.TOKENS.INVERSE: opcode = (int) Global.OP_CODES.INVERSE; break;
                }
            }

            return new Operation(opcode, x, y, m);
        }
    }
}