﻿using System;
using System.IO;
using System.Collections.Generic;

namespace integer_arithmetic
{
    class Program
    {
        static void Main(string[] args)
        {
            // check if a filename was given (for example: dotnet run example.txt)
            if (args.Length == 0) {
               Console.WriteLine("Please provide an input file");
               return;
            }

            // get filename from commandline arguments
            string fname = args[0];

            // create new Parser that will parse the inputfile
            Parser p = new Parser(fname);

            // List of executable operations from the file
            List<Operation> operations = p.parse();
            
            // Execute the operations
            operations.ForEach(op => op.execute());

            // collect the output
            string output = "";
            operations.ForEach(op => output = output + op.generateOutput() + Environment.NewLine + Environment.NewLine);

            File.WriteAllText("output.txt", output);
        }
    }
}
