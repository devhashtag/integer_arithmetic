using System;
using System.Linq;
using System.Collections.Generic;

namespace integer_arithmetic
{
    class Number
    {
        private int radix;
        private List<int> value;
        // true if positive, false if negative
        private bool sign;

        public Number(List<int> value, int radix = 10, bool sign = true)
        {
            // initialize value
            this.value = value;

            // initialize radix
            this.radix = radix;
            // max 16
            this.radix = this.radix <= 16 ? this.radix : 16;
            // min 2
            this.radix = this.radix >= 2 ? this.radix : 2;

            // set sign
            this.sign = sign;
        }

        public int getRadix()
        {
            return this.radix;
        }

        public void setRadix(int radix)
        {
            this.radix = radix;

            // TODO: convert value to new radix
        }

        public List<int> getValue() {
            return this.value;
        }

        public void setValue(List<int> value) {
            // implement some error checking
            this.value = value;
        }

        // returns true if the value is 0 (or if the value does not have any digits)
        public bool isZero() {
            return this.value.Count == 0 || this.value.All((int x) => x == 0);
        }

        // Converts a string into an instance of Number
        public static Number fromString(string number, int radix) {
            List<int> value = new List<int>();
            bool sign = true;

            // check sign
            if (number.Length > 0 && number[0] ==  '-') {
                sign = false;
                // remove from string for the foreach loop
                number = number.Substring(1);
            }

            // convert to uppercase, because the dictionaries in Global use uppercase characters as keys
            number = number.ToUpper();

            foreach (char c in number) {
                if (c == '-') {
                    sign = true;
                } else {
                    value.Add(Global.DIGITS[c.ToString()]);
                }
            }

            value.Reverse();

            return new Number(value, radix, sign);
        }

        public string toString()
        {
            // remove leading zeroes
            for (int i = this.value.Count - 1; i > 0; i--) {
                if (this.value[i] == 0) {
                    this.value.RemoveAt(i);
                } else {
                    break;
                }
            }

            string tmp = "";
            this.value.ForEach((int x) => tmp = Global.SYMBOLS[x] + tmp);
            // only negative when the sign is false and the value is not zero
            tmp = this.sign ? tmp : this.isZero() ? tmp : "-" + tmp;
            return tmp;
        }

        // creates a Number with value 0
        public static Number Zero(int radix = 10) {
            return new Number(new List<int>() {0}, radix);
        }

        public static Number One(int radix = 10) {
            return new Number(new List<int>() {1}, radix);
        }

        public Number Clone() {
            return new Number(value.Select(x => x).ToList(), this.radix, this.sign);
        }

        public void setSign (bool b) {
            this.sign = b;
        }

        public bool getSign() {
            return this.sign;
        }

        public static Number operator +(Number a, Number b)
        {   
            // if a is negative and b is positive, -a + b = b - a
            if (!a.sign && b.sign) {
                Number _a = a.Clone();
                _a.sign = true;

                Number res = b - _a;
                return res;
            }

            // if a is positive and b is negative, a + b = a - |b|
            if (a.sign && !b.sign) {
                Number _b = b.Clone();
                _b.sign = true;

                return a - _b;
            }

            // return null if the two radixes are different
            if (a.getRadix() != b.getRadix()) {
                return null;
            }
 
            //Make both numbers the same length
            int max = Math.Max(a.value.Count, b.value.Count);
            
            while (a.value.Count != max) {
                a.value.Add(0);
            }
            
             while (b.value.Count != max) {
                b.value.Add(0);
            }

            List<int> zList = new List<int>();

            int c = 0;    
            for (int i = 0; i < max; i++) {
                int z = a.value[i] + b.value[i] + c;
                Global.additions += 2;
                if(z >= a.getRadix()) {
                    z -= a.getRadix();
                    c = 1; 
                }
                else {
                    c = 0;
                }
                zList.Add(z);
            }

            if (c == 1) {
                zList.Add(1);
            }

            Number result = new Number(zList, a.getRadix());

            // if a and b are both negative, the result is also negative -(|a| + |b|)
            if (!a.sign && !b.sign) {
                result.sign = false;
            }

            return result;
        }

        public static Number operator *(Number a, Number b)
        {
            // return null if the two radixes are different
            if (a.getRadix() != b.getRadix()) {
                return null;
            }

            bool neg = false;
            if (a.sign ^ b.sign) {
                neg = true;
            }

            int max = Math.Max(a.value.Count, b.value.Count);

            while (a.value.Count != max) {
                a.value.Add(0);
            }

            while (b.value.Count != max) {
                b.value.Add(0);
            }
            
            List<int> zList = new List<int>();
            Number z = new Number(zList, a.getRadix());

            while (z.value.Count != (a.value.Count + b.value.Count)) {
                z.value.Add(0);
            }

            for (int i = 0; i < a.value.Count; i++) {
                int c = 0;
                for (int j = 0; j < b.value.Count; j++) {
                    int t = z.value[i+j] + a.value[i]*b.value[j] + c;
                    c = (int)Math.Floor((double)t / b.getRadix());
                    z.value[i+j] = t - c * b.getRadix();
                    Global.multiplications++;
                    Global.additions += 2;
                }
                z.value[i + b.value.Count] = c;
            }
            
            int k = a.value.Count + b.value.Count - 1;
            while (z.value[k] == 0 && k > 0) {
                z.value.RemoveAt(k);
                k--;
            }

            if (neg) {
                z.sign = false;
            }

            return z;
        }

        // public static Number operator /(Number a, Number b) {
        //     //Initialise remainder and counter
        //     Number r = a;
        //     //Add a leading zero
        //     r.value.Add(0);
        //     int k = a.value.Count - b.value.Count + 1;

        //     //Initialise list for q
        //     List<int> qList = new List<int>();

        //     //Do the division
        //     for (int i = k - 1; k >= 0; k--) {
        //         int q = (int)Math.Floor(((double)r.value[i + b.value.Count] * (double)r.getRadix()) / (double)b.value[b.value.Count - 1]);
        //         if (q >= a.getRadix()) {
        //             q = a.getRadix() - 1;
        //         }
        //         int c = 0;
        //         for (int j = 0; j < b.value.Count; j++) {
        //             int tmp = r.value[i + j] + q * b.value[i] + c;
        //             c = tmp / a.getRadix();
        //             r.value[i + j] = tmp % a.getRadix();
        //         }
        //         r.value[i + b.value.Count] = r.value[i + b.value.Count] + c;

        //         while (r.value[i + b.value.Count] < 0) {
        //             c = 0;
        //             for (int j = 0; j < b.value.Count; j++) {
        //                 int tmp = r.value[i + j] + b.value[i] + c;
        //                 c = tmp / a.getRadix();
        //                 r.value[i + j] = tmp % a.getRadix();
        //             }
        //             r.value[i + b.value.Count] = r.value[i + b.value.Count] + c;
        //             q = q - 1;
        //         }

        //         qList.Add(q);
        //     }

        //     Number result = new Number(qList, a.getRadix());

        //     return result;
        // }

        public static Number operator /(Number a, Number b) {
            if (b.isZero()) {
                throw new Exception("Cannot divide by zero");
            }

            // res = 0
            Number res = Number.Zero(a.getRadix());

            // we always set the sign to true
            Number n = a.Clone();
            n.sign = true;

            Number m = b.Clone();
            m.sign = true;

            // will subtract b from a until a is less than b
            while (n >= m) {
                n = n - m;
                res++;
            }

            // set the sign (not (a XOR b))
            res.sign = !(a.sign ^ b.sign);

            return res;
        }

        public static Number operator -(Number d, Number e)
        {
            Number a = d.Clone();
            Number b = e.Clone();
            // return null if the two radixes are different
            if (a.getRadix() != b.getRadix()) {
                return null;
            }

            //Make both numbers the same length
            int max = Math.Max(a.value.Count, b.value.Count); //Largest word length
            while (a.value.Count != max) {
                a.value.Add(0);
            }

            while (b.value.Count != max) {
                b.value.Add(0);
            }

            //Define numbers to do the calculation with
            Number x = new Number(new List<int>());
            Number y = new Number(new List<int>());

            //Create boolean to record whether the result will be negative
            bool neg = false;

            if (a == b) { //Check if a = b, if so return 0.
                return Number.Zero(a.getRadix());
            } else if (!a.sign && b.sign) { //Then check if one of them is negative, if so add them as if they were both positive and make it negative. 
                a.sign = true;
                b.sign = true;
                Number z = a + b;
                z.sign = false;
                return z;
            } else if (a.sign && !b.sign) { //Then check if one of them is negative, if so add them as if they were both positive and make it negative. 
                a.sign = true;
                b.sign = true;
                Number z = a + b;
                z.sign = true;
                return z;
            } else if (a >= b && a.sign && b.sign) { //Check if a >= b and both positive. Do a - b
                x = a;
                y = b;
            } else if (a >= b && !a.sign && !b.sign){ //Check if a>= b and both negative. Do b - a
                y = a;
                x = b;
            } else if (a < b && a.sign && b.sign) { //Check if a < b and both positive. Do b - a and make the result negative
                y = a;
                x = b;
                neg = true;
            } else { //a < b and both negative must hold. Do a - b and make the result negative
                x = b;
                y = a;
                neg = true;
            }

            //Create list to house the digits of z
            List<int> zList = new List<int>();
            //Set carry to 0
            int c = 0;

            //For each digit from 0 to the largest length
            for (int i = 0; i < max; i++) {
                int z = x.value[i] - y.value[i] - c; //Do x - y for digit i
                if(z < 0) { //If result is less than 0
                    z += x.getRadix(); //Make z positive
                    c = 1; //Set the carry to 1
                }
                else { //If not
                    c = 0; //Reset carry
                }
                zList.Add(z); //Add digit to the list
            }
            //Create a new Number class to house the result
            Number result = new Number(zList, a.getRadix());

            //Remove leading zeroes
            int j = result.value.Count - 1;
            while (result.value[j] == 0 && j > 0) {
                result.value.RemoveAt(j);
                j--;
            }
            
            //Check if the result should be negative and make it negative if it should be.
            if (neg) {
                result.sign = false;
            }
            
            return result;
        }

        // modular reduction
        public static Number operator % (Number x, Number m) {
            // declare output
            Number y;

            // x' <- |x|
            Number _x = x.Clone();
            _x.sign = true;
            // k <- word length of x
            int k = x.value.Count;
            // n <- word length of m
            int n = m.value.Count;
            // for i = k - n down to 0
            for (int i = k - n; i >= 0; i--) {
                // while (_x >= m * b^i) _x = _x - m*b^i;
                while (_x >= m << i) _x = _x - (m << i);
            }

            // if x >= 0 or x' = 0
            if (x >= Number.Zero(x.getRadix()) || _x == Number.Zero(_x.getRadix())) {
                y = _x.Clone();
            } else {
                y = m - _x;
            }

            // strip leading zeroes
            return Number.fromString(y.toString(), y.radix);
        }

        public static Number karatsuba (Number x, Number y)
        {
            Number a = x.Clone();
            Number b = y.Clone();
            if (a.getRadix() != b.getRadix()) {
                return null;
            }

            //Check if length of both numbers is 1 and if so, do the multiplication right away and return the result
            if (a.value.Count == 1 && b.value.Count == 1) {
                int result = a.value[0] * b.value[0];
                Number r = null;
                Global.multiplications++; //Add 1 to the multiplication counter
                if (result > a.getRadix()) { //If the result is larger than the base, ensure the digits are saved seperately instead of together
                    int high = result / a.getRadix();
                    int low = result % a.getRadix();
                    List<int> resultList = new List<int>();
                    resultList.Add(low);
                    resultList.Add(high);
                    r = new Number (resultList, a.getRadix());
                }
                else { //Return the result as a new Number class
                    List<int> rList = new List<int>();
                    rList.Add(result);
                    r = new Number (rList, a.getRadix());
                }

                if (a.sign ^b.sign) {
                    r.sign = false;
                }
                
                return r;
            }

            //Check if a has even length and make it even if not
            if(a.value.Count % 2 != 0) {
                a.value.Add(0);
            }

            //Check if b has even length and make it even if not
            if(b.value.Count % 2 != 0) {
                b.value.Add(0);
            }

            //Check which number is the largest and make the other number as large by adding zeroes
            int max = Math.Max(a.value.Count, b.value.Count);

            while (a.value.Count != max) {
               a.value.Add(0); 
            }

            while (b.value.Count != max) {
                b.value.Add(0);
            }

            int half = max / 2; // n/2 in Karatsuba's algorithm, n being the length of the number (in here max)

            List<int> ahiList = new List<int>();
            List<int> alowList = new List<int>();
            List<int> bhiList = new List<int>();
            List<int> blowList = new List<int>();

            //Split a and b up into two parts: a = ahi * radix^half + alow * radix^0 and b = bhi * radix^half + blow * radix^0
            for (int k = 0; k < a.value.Count; k++) {
               if (k < half) {
                    alowList.Add(a.value[k]);
                    blowList.Add(b.value[k]);
               } else {
                    ahiList.Add(a.value[k]);
                    bhiList.Add(b.value[k]);
               }
            }

            //Declare the new Numbers ahi, alow, bhi, blow
            Number ahi = new Number(ahiList, a.getRadix());
            Number alow = new Number(alowList, a.getRadix());
            Number bhi = new Number(bhiList, b.getRadix());
            Number blow = new Number(blowList, b.getRadix());

            //Recurse over the multiplications needed to combine everything
            Number A = karatsuba (ahi, bhi);
            Number B = karatsuba (alow, blow);
            Number C = karatsuba((ahi + alow), (blow + bhi));
            // additions are counted in the + operator, so we dont have to do it here

            //Calculate (ahi * blow) + (alow * bhi)
            Number D = C - A - B;

            // Digit shift of A, shifting the digits by n
            A = A << max;

            // Digit shift of D, shifting the digits by n/2
            D = D << half;

            // Calculate the result of a * b
            Number c = A + D + B;
            // again, additions are counted in the + operator

            //See if there are leading zeroes and if so delete them except for the very last one (in case the result is 0)
            int j = c.value.Count - 1;
            while (c.value[j] == 0 && j > 0) {
                c.value.RemoveAt(j);
                j--;
            }

            if (a.sign ^ b.sign) {
                c.sign = false;
            }
            return c;
        }

        public static bool operator == (Number a, Number b) {
            if (object.ReferenceEquals(a, null) ^ object.ReferenceEquals(b, null)) {
                return false;
            }

            if (object.ReferenceEquals(a, null) && object.ReferenceEquals(b, null)) {
                return true;
            }

            if (a.getRadix() != b.getRadix()) {
                return false;
            }

            int min = Math.Min(a.getValue().Count, b.getValue().Count);

            for (int i = 0; i < min; i++) {
                if (a.getValue()[i] != b.getValue()[i])
                    return false;
                
            }

            if (min == a.getValue().Count) {
                // b has more entries in its value List
                int i = min;

                while (i < b.getValue().Count) {
                    if (b.getValue()[i] != 0) return false;
                    i++;
                }
            } else {
                // a has more entries in its value List
                int i = min;

                while (i < a.getValue().Count) {
                    if (a.getValue()[i] != 0) return false;
                    i++;
                }
            }
            return true;
        }

        public static bool operator != (Number a, Number b) {
            return !(a == b);
        }

        public static bool operator < (Number a, Number b) {
            // just in case
            if (a.isZero() && !a.sign) {
                a.sign = true;
            }

            if (b.isZero() && !b.sign) {
                b.sign = true;
            }

            int n = a.value.Count - 1;
            int m = b.value.Count - 1;

            // if a is negative and b is positive, a < b
            if (!a.sign && b.sign) {
                return true;
            }

            // if a is positive and b is negative, a > b
            if (a.sign & !b.sign) {
                return false;
            }

            // if both are negative, then a < b is true iff |a| > |b| (or: !(|a| < |b|))
            if (!a.sign && !b.sign) {
                Number _a = a.Clone();
                Number _b = b.Clone();
                _a.sign = true;
                _b.sign = true;
                return !(_a < _b);
            }

            // if a has more digits than b, check if the extra are not zero
            // if not, then a > b
            if (n > m) {
                for (int i = n; i > m; i--) {
                    if (a.value[i] != 0) {
                        return false;
                    }
                }
            }

            // if b has more digits than a, check if the extra are not zero
            // if not, then b > a
            if (n < m) {
                for (int i = m; i > n; i--) {
                    if (b.value[i] != 0) {
                        return true;
                    }
                }
            }

            // numbers are of equal length (or extra digits are all 0)
            int idx = Math.Min(n, m);

            // check for each digit if it is the same
            // if not, then return true of digit of a is smaller than the digit of b and false otherwise
            while (idx >= 0) {
                if (a.value[idx] != b.value[idx]) {
                    return a.value[idx] < b.value[idx];
                }
                idx--;
            }

            // if the code comes here, the numbers must be equal or a < b
            return a == b ? false : true;
        }

        public static bool operator > (Number a, Number b) {
            // just in case
            if (a.isZero() && !a.sign) {
                a.sign = true;
            }

            if (b.isZero() && !b.sign) {
                b.sign = true;
            }

            int n = a.value.Count - 1;
            int m = b.value.Count - 1;


            // if a is negative and b is positive, a < b
            if (!a.sign && b.sign) {
                return false;
            }

            // if a is positive and b is negative, a > b
            if (a.sign & !b.sign) {
                return true;
            }

            // if both are negative, then a > b is true iff |a| < |b| (or: !(|a| > |b|))
            if (!a.sign && !b.sign) {
                Number _a = a.Clone();
                Number _b = b.Clone();
                _a.sign = true;
                _b.sign = true;
                return !(_a > _b);
            }

            // if a has more digits than b, check if the extra are not zero
            // if not, then a > b
            if (n > m) {
                for (int i = n; i > m; i--) {
                    if (a.value[i] != 0) {
                        return true;
                    }
                }
            }

            // if b has more digits than a, check if the extra are not zero
            // if not, then b > a
            if (n < m) {
                for (int i = m; i > n; i--) {
                    if (b.value[i] != 0) {
                        return false;
                    }
                }
            }

            // numbers are of equal length (or extra digits are all 0)
            int idx = Math.Min(n, m);

            // check for each digit if it is the same
            // if not, then return true if digit of a is greater than the digit of b and false otherwise
            while (idx >= 0) {
                if (a.value[idx] != b.value[idx]) {
                    return a.value[idx] > b.value[idx];
                }
                idx--;
            }

            // if the code comes here, the numbers must be equal or a > b
            return a == b ? false : true;
        }

        public static bool operator <= (Number a, Number b) {
            return !(a > b);
        }

        public static bool operator >= (Number a, Number b) {
            return !(a < b);
        }

        public static Number operator << (Number a, int c) {
            Number b = a.Clone();

            while (c-- > 0) {
                b.value.Insert(0, 0);
            }

            return b;
        }

        public static Number operator >> (Number a, int c) {
            Number b = a.Clone();

            for (int i = 0; i < b.value.Count - c; i++) {
                b.value[i] = b.value[i + c];
            }

            // remove leading zeroes
            b.value.RemoveRange(b.value.Count - c - 1, c);

            return b;
        }

        public static Number operator ++(Number a) {
            return a + Number.One(a.getRadix());
        }

        public static Number operator --(Number a) {
            return a - Number.One(a.getRadix());
        }

        public static implicit operator Number(int v)
        {
            throw new NotImplementedException();
        }

        public static Number modAddition (Number a, Number b, Number m) {
            //Make the number 2 into a Number class
            Number minMod = Number.fromString("2", a.getRadix());

            //Check if the mod is less than 2
            if (m < minMod) {
                return null; //If so return null
            }

            Number z = (a + b) % m;
            Number result = new Number(new List<int>(), z.getRadix());

            //Check if z < m , if so return z if not subtract m to z and return it
            if (z < m) {
                result = z;
            } else {
                result = z - m;
            }
            return result;
        }

        public static Number modSubtraction (Number a, Number b, Number m) {
            //Make the number 2 into a Number class
            Number minMod = Number.fromString("2", a.getRadix());

            //Check if the mod is less than 2
            if (m < minMod) {
                return null; //If so return null
            }

            Number z = (a - b) % m;
            Number result = new Number(new List<int>(), z.getRadix());

            //Check if z > 0, if so return z if not add m to z and return it
            if (z >= Number.Zero(z.getRadix())) {
                result = z;
            } else {
                result = z + m;
            }
            return result;
        }

        public static Number modInverse (Number a, Number m) {
            Number minMod = Number.fromString("2", a.getRadix());

            if (m < minMod) {
                return null;
            }

            (Number d, Number x, Number y) = extendedEucledian(a, m);
            
            if (d == Number.One(d.getRadix())) {
                return x % m;
            } else {
                return null;
            }

        }

        public static Tuple<Number, Number, Number> extendedEucledian (Number a, Number b) {
            Number a_prime = a.Clone();
            Number b_prime = b.Clone();
            Number x1 = Number.One(a.getRadix());
            Number x2 = Number.Zero(a.getRadix());
            Number y1 = Number.Zero(a.getRadix());
            Number y2 = Number.One(a.getRadix());
            a_prime.sign = true;
            b_prime.sign = true;

            while (b_prime > Number.Zero(a.getRadix())) {
                Number q = a_prime / b_prime;
                Number r = a_prime - karatsuba(q, b_prime);
                a_prime = b_prime.Clone();
                b_prime = r.Clone();
                Number x3 = x1 - karatsuba(q, x2);
                Number y3 = y1 - karatsuba(q, y2);
                x1 = x2.Clone();
                y1 = y2.Clone();
                x2 = x3.Clone();
                y2 = y3.Clone();
            }

            Number d = a_prime.Clone();
            Number x = x1.Clone();
            Number y = y1.Clone();

            if (a <= Number.Zero(a.getRadix())) {
                x.sign = false;
            }
            
            if (b <= Number.Zero(a.getRadix())) {
                y.sign = false;
            }

            int j = d.value.Count - 1;
            while (d.value[j] == 0 && j > 0) {
                d.value.RemoveAt(j);
                j--;
            }

            int k = x.value.Count - 1;
            while (x.value[k] == 0 && k > 0) {
                x.value.RemoveAt(k);
                k--;
            }

            int l = y.value.Count - 1;
            while (y.value[l] == 0 && l > 0) {
                y.value.RemoveAt(l);
                l--;
            }
            
            return Tuple.Create(d, x, y);
        }

        public static Number modMultiplication (Number x, Number y, Number m) {
            Number a = x.Clone();
            Number b = y.Clone();

            return karatsuba(a,b) % m;
        }
    }
}