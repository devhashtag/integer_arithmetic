using System.Linq;
using System.Collections.Generic;

/*
    each line in input is a token
    

 */

public class Global {
        // used for converting a Number into a string
        public static readonly string[] SYMBOLS = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};

        // used for converting a string into a Number
        public static readonly Dictionary<string, int> DIGITS = new Dictionary<string, int>() {
            {"0", 0},
            {"1", 1},
            {"2", 2},
            {"3", 3},
            {"4", 4},
            {"5", 5},
            {"6", 6},
            {"7", 7},
            {"8", 8},
            {"9", 9},
            {"A", 10},
            {"B", 11},
            {"C", 12},
            {"D", 13},
            {"E", 14},
            {"F", 15}};

        // used in the Operation class to denote the different operations we can do on Numbers
        public enum OP_CODES {ADD, SUBTRACT, MULTIPLY, KARATSUBA, REDUCE, INVERSE, EUCLID, MODULAR_ADD, MODULAR_SUBTRACT, MODULAR_MULTIPLY};
        public static int additions = 0;
        public static int multiplications = 0;

        // Maps instructions to ints, used in the parser
        public enum TOKENS {ADD, SUBTRACT, MULTIPLY, KARATSUBA, REDUCE, INVERSE, EUCLID, RADIX, X, Y, M};

        // Maps strings to tokens, used in the parser
        public static Dictionary<string, int> TOKEN_VALUES = new Dictionary<string, int>() {
            {"[add]", 0},
            {"[subtract]", 1},
            {"[multiply]", 2},
            {"[karatsuba]", 3},
            {"[reduce]", 4},
            {"[inverse]", 5},
            {"[euclid]", 6},
            {"[radix]", 7},
            {"[x]", 8},
            {"[y]", 9},
            {"[m]", 10}
        };

        // Maps operations values back to strings, for output
        public static Dictionary<int, string> OPERATION_SYMBOLS = new Dictionary<int, string>() {
            {0, "[add]"},
            {1, "[subtract]"},
            {2, "[multiply]"},
            {3, "[karatsuba]"},
            {4, "[reduce]"},
            {5, "[inverse]"},
            {6, "[euclid]"},
            {7, "[add]"},
            {8, "[subtract]"},
            {9, "[karatsuba]"}
        };

        // Maps token values back to strings, also for ouput
        public static Dictionary<int, string> TOKEN_SYMBOLS = new Dictionary<int, string>() {
            {0, "[add]"},
            {1, "[subtract]"},
            {2, "[multiply]"},
            {3, "[karatsuba]"},
            {4, "[reduce]"},
            {5, "[inverse]"},
            {6, "[euclid]"},
            {7, "[radix]"},
            {8, "[x]"},
            {9, "[y]"},
            {10, "[m]"}
        };

        // returns whether or not an instruction is an operation that can be done on Numbers
        public static bool isOperation(int instruction) {
            switch (instruction) {
                case (int) TOKENS.ADD: return true;
                case (int) TOKENS.SUBTRACT: return true;
                case (int) TOKENS.MULTIPLY: return true;
                case (int) TOKENS.KARATSUBA: return true;
                case (int) TOKENS.REDUCE: return true;
                case (int) TOKENS.INVERSE: return true;
                case (int) TOKENS.EUCLID: return true;
                default: return false;
            }
        }
    }
