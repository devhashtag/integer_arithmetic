using System;

namespace integer_arithmetic {
    class Token {
        private int token;
        private string argument;
        public Token(int token, string argument = "") {
            this.token = token;
            this.argument = argument;
        }

        public int getToken() {
            return this.token;
        }

        public string getArgument() {
            return this.argument;
        }
    }
}